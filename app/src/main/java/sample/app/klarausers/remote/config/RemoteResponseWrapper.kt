package sample.app.klarausers.remote.config

import com.google.gson.annotations.SerializedName

sealed class ResponseWrapper<T>

data class Success<T>(val payload: T?) : ResponseWrapper<T>()

data class Failure<T>(@SerializedName("code") val code: Int = INVALID_CODE,
                      @SerializedName("message") val message: String = ""
) : ResponseWrapper<T>()

class NoInternet<T> : ResponseWrapper<T>()

const val INVALID_CODE = -1

data class GenericErrorException(val error: Failure<*>) : Exception(error.toString())