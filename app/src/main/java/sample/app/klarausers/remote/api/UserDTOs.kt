package sample.app.klarausers.remote.api

import com.google.gson.annotations.SerializedName

data class UserInfoDto(
        @SerializedName("id") val userId: String?,
        @SerializedName("firstName") val firstName: String?,
        @SerializedName("lastName") val lastName: String?,
        @SerializedName("phoneNumber") val phoneNumber: String?,
        @SerializedName("email") val email: String?,
        @SerializedName("profilePicture") val profilePicture: String?
)