package sample.app.klarausers.remote.repository

import io.reactivex.Single
import sample.app.klarausers.remote.RemoteStreamProcessor
import sample.app.klarausers.remote.api.UserInfoDto
import sample.app.klarausers.remote.api.UserInfoService
import sample.app.klarausers.remote.config.ResponseWrapper

class GetUserInfoRepository(private val getUserInfoDataSourse: GetUserInfoDataSource = GetUserInfoDataSource()) {

    fun getUsersInfo(): Single<ResponseWrapper<UserInfoDto>> {
        return getUserInfoDataSourse.getUserInfo()
    }
}

class GetUserInfoDataSource : RemoteStreamProcessor() {

    fun getUserInfo(): Single<ResponseWrapper<UserInfoDto>> {
        val service = createService(UserInfoService::class.java)
        return processApiStream(service.getUserInfo())
    }
}