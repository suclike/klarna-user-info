package sample.app.klarausers.remote.api

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET

interface UserInfoService {

    @GET("all")
    fun getUserInfo(): Single<Response<UserInfoDto>>
}