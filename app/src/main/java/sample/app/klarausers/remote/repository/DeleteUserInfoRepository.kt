package sample.app.klarausers.remote.repository

import io.reactivex.Single
import sample.app.klarausers.remote.RemoteStreamProcessor
import sample.app.klarausers.remote.api.DeleteUserInfoService
import sample.app.klarausers.remote.config.ResponseWrapper

class DeleteUserInfoRepository(private val deleteUserInfoDataSource: DeleteUserInfoDataSource = DeleteUserInfoDataSource()) {

    fun deleteUser(userId: String): Single<ResponseWrapper<Void>> {
        return deleteUserInfoDataSource.deleteUser(userId)
    }
}

class DeleteUserInfoDataSource : RemoteStreamProcessor() {

    fun deleteUser(userId: String): Single<ResponseWrapper<Void>> {
        val service = createService(DeleteUserInfoService::class.java)
        return processApiStream(service.deleteUser(userId))
    }
}