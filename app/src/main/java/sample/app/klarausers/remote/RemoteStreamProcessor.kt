package sample.app.klarausers.remote

import io.reactivex.Single
import retrofit2.Response
import sample.app.klarausers.remote.config.Failure
import sample.app.klarausers.remote.config.NoInternet
import sample.app.klarausers.remote.config.ResponseWrapper
import sample.app.klarausers.remote.config.Success
import sample.app.klarausers.remote.config.retrofit

abstract class RemoteStreamProcessor {

    internal fun <T> processApiStream(apiStream: Single<Response<T>>): Single<ResponseWrapper<T>> =
            apiStream
                    .flatMap { currentResponse -> processResponse(response = currentResponse) }
                    .onErrorReturn { error ->
                        Failure(message = error.localizedMessage)
                    }

    private fun <T> processResponse(response: Response<T>?): Single<ResponseWrapper<T>> =
            response?.let {
                Single.just<ResponseWrapper<T>>(
                        if (response.isSuccessful) {
                            Success(payload = response.body())
                        } else {
                            Failure<T>(code = response.code(), message = response.message())
                        }
                )
            } ?: Single.just(NoInternet())

    internal fun <S> createService(clazz: Class<S>) = retrofit.create(clazz)
}