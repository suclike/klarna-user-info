package sample.app.klarausers.remote.api

import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.DELETE
import retrofit2.http.Path

interface DeleteUserInfoService {

    @DELETE("{id}")
    fun deleteUser(@Path("id") id: String): Single<Response<Void>>
}