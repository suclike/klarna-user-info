package sample.app.klarausers.domain

import io.reactivex.Single
import sample.app.klarausers.remote.api.UserInfoDto
import sample.app.klarausers.remote.config.Failure
import sample.app.klarausers.remote.config.GenericErrorException
import sample.app.klarausers.remote.config.Success
import sample.app.klarausers.remote.repository.GetUserInfoRepository
import sample.app.klarausers.widget.UserInfoModel

class GetUserInfoAction(private val getAllUsersRepository: GetUserInfoRepository = GetUserInfoRepository()) {

    fun getUserInfo(): Single<UserInfoModel> {
        return getAllUsersRepository.getUsersInfo()
                .flatMap { responseWrapper ->
                    when (responseWrapper) {
                        is Success -> Single.just(mapDtoToModel(responseWrapper))
                        else -> Single.error(GenericErrorException(responseWrapper as Failure<*>))
                    }
                }
                .onErrorReturn {
                    emptyUserInfoModel
                }
    }
}

val mapDtoToModel: (userInfoDtoResponseWrapper: Success<UserInfoDto>) -> UserInfoModel =
        { userInfoDtoResponseWrapper: Success<UserInfoDto> ->
            userInfoDtoResponseWrapper.payload?.let { userInfoDto ->
                userInfoDto.let { userDto ->
                    with(userDto) {
                        UserInfoModel(
                                userId = userId.orEmpty(),
                                firstName = firstName.orEmpty(),
                                lastName = lastName.orEmpty(),
                                phoneNumber = phoneNumber.orEmpty(),
                                emailAddress = email.orEmpty(),
                                profileImageUrl = profilePicture.orEmpty()
                        )
                    }
                }
            } ?: emptyUserInfoModel
        }

private val emptyUserInfoModel = UserInfoModel()