package sample.app.klarausers.domain

import io.reactivex.Single
import sample.app.klarausers.remote.config.Failure
import sample.app.klarausers.remote.config.GenericErrorException
import sample.app.klarausers.remote.config.Success
import sample.app.klarausers.remote.repository.DeleteUserInfoRepository

class DeleteUserInfoAction(private val deleteUserInfoRepository: DeleteUserInfoRepository =
                                   DeleteUserInfoRepository()) {

    fun deleteUser(userId: String): Single<Boolean> {
        return deleteUserInfoRepository.deleteUser(userId)
                .flatMap { responseWrapper ->
                    when (responseWrapper) {
                        is Success -> Single.just(true)
                        else -> Single.error(GenericErrorException(responseWrapper as Failure<*>))
                    }
                }
                .onErrorReturn {
                    false
                }
    }
}
