package sample.app.klarausers.widget.listener

interface OnDeleteButtonClickListener {
    fun onDeleteClicked(userId: String)
}