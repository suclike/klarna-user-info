package sample.app.klarausers.widget

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import sample.app.klarausers.domain.DeleteUserInfoAction
import sample.app.klarausers.domain.GetUserInfoAction
import sample.app.klarausers.util.SingleLiveEvent
import java.util.concurrent.TimeUnit

class MainViewModel(private val getAllUsersAction: GetUserInfoAction = GetUserInfoAction(),
                    private val deleteUserInfoAction: DeleteUserInfoAction = DeleteUserInfoAction()
) : ViewModel() {

    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val dispatchLoadedUser = MutableLiveData<UserInfoModel>()

    private val notifyIfUserRemoved = SingleLiveEvent<Boolean>()

    private val notifyErrorOccurred = SingleLiveEvent<String>()

    fun fetchRemoteUserInfo() {
        getCompositeDisposable().add(
                getAllUsersAction.getUserInfo()
                        .subscribeOn(Schedulers.io())
                        .delay(1500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ userInfoModel ->
                            onUserInfoLoaded(userInfoModel)
                        }, {
                            onErrorOccurred(it.localizedMessage)
                        })
        )
    }

    fun subscribeWhenUserLoaded() = dispatchLoadedUser

    fun subscribeWhenUserRemoved() = notifyIfUserRemoved

    fun subscribeWhenErrorOccurred() = notifyErrorOccurred

    fun deleteUser(userId: String) {
        if (userId.isNotEmpty()) {
            getCompositeDisposable().add(
                    deleteUserInfoAction.deleteUser(userId)
                            .subscribeOn(Schedulers.io())
                            .delay(1500, TimeUnit.MILLISECONDS)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe({ isUserRemoved ->
                                onUserInfoRemoved(isUserRemoved)
                            }, {
                                onErrorOccurred(it.localizedMessage)
                            })
            )
        } else {
            onUserInfoRemoved(false)
        }
    }

    private fun onUserInfoLoaded(userInfoModel: UserInfoModel) {
        dispatchLoadedUser.value = userInfoModel
    }

    private fun onUserInfoRemoved(userRemoved: Boolean) {
        notifyIfUserRemoved.value = userRemoved
    }

    private fun onErrorOccurred(localizedMessage: String) {
        notifyErrorOccurred.value = localizedMessage
    }

    private fun getCompositeDisposable(): CompositeDisposable {
        if (compositeDisposable.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }

        return compositeDisposable
    }
}