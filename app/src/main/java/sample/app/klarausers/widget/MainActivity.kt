package sample.app.klarausers.widget

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import kotlinx.android.synthetic.main.activity_main.*
import sample.app.klarausers.R
import sample.app.klarausers.widget.adapter.UsersRecyclerViewAdapter
import sample.app.klarausers.widget.listener.OnDeleteButtonClickListener
import sample.app.klarausers.widget.listener.OnLoadButtonClickListener

class MainActivity : AppCompatActivity(), OnLoadButtonClickListener,
        OnDeleteButtonClickListener {

    private val infoItemsAdapter: UsersRecyclerViewAdapter by lazy {
        UsersRecyclerViewAdapter.createAdapter(onLoadClickListener = this,
                onDeleteButtonClickListener = this)
    }

    private val viewModel: MainViewModel by lazy {
        ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    private val defaultListOfItems by lazy {
        listOf(UserInfoLoadingModel, UserInfoModel())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecycleView()
    }

    override fun onStart() {
        super.onStart()

        with(viewModel) {
            subscribeWhenUserLoaded()
                    .observe(this@MainActivity, userInfoLoadedObserver)

            subscribeWhenUserRemoved()
                    .observe(this@MainActivity, userRemovedObserver)

            subscribeWhenErrorOccurred()
                    .observe(this@MainActivity, errorOccurredObserver)
        }
    }

    override fun onStop() {
        super.onStop()

        with(viewModel) {
            subscribeWhenUserLoaded()
                    .removeObserver(userInfoLoadedObserver)

            subscribeWhenUserRemoved()
                    .removeObserver(userRemovedObserver)

            subscribeWhenErrorOccurred()
                    .removeObserver(errorOccurredObserver)
        }

    }

    override fun onLoadClicked() {
        showLoadingProgress()
        viewModel.fetchRemoteUserInfo()
    }

    override fun onDeleteClicked(userId: String) {
        viewModel.deleteUser(userId)
    }

    private fun initRecycleView() {
        mainRecyclerView.init()
                .also {
                    infoItemsAdapter.setAdapterDate(defaultListOfItems)
                    it.adapter = infoItemsAdapter
                }
    }

    private val userInfoLoadedObserver = Observer<UserInfoModel> { userInfo ->
        userInfo?.let {
            if (!userInfo.isDefault) {
                mainRecyclerView.smoothScrollToPosition(infoItemsAdapter.getLastItem())
                mainRecyclerView?.postDelayed({
                    hideLoadingProgress()
                    infoItemsAdapter.updateItems(userInfo)
                }, 500)
            } else {
                hideLoadingProgress()
                showToastWithText("Failed to load user data")
            }
        }
    }

    private val userRemovedObserver = Observer<Boolean> { isRemoved ->
        isRemoved?.let { shouldRemoveUser ->
            when {
                shouldRemoveUser -> {
                    mainRecyclerView?.post {
                        infoItemsAdapter.removeExistingUser()
                        mainRecyclerView.scrollToPosition(0)
                    }
                }
                else -> showToastWithText("Failed to remove user")
            }
        }
    }

    private val errorOccurredObserver = Observer<String> { errorMessage ->
        errorMessage?.also { message ->
            showToastWithText(message)
        }
    }

    private fun showToastWithText(message: String) {
        Toast.makeText(this@MainActivity, message, LENGTH_SHORT).show()
    }

    private fun showLoadingProgress() {
        dataLoadingAnimationView.visibility = View.VISIBLE
        dataLoadingAnimationView.playAnimation()
    }

    private fun hideLoadingProgress() {
        dataLoadingAnimationView.visibility = View.GONE
        dataLoadingAnimationView.cancelAnimation()
    }

}

private fun RecyclerView.init(layoutManager: RecyclerView.LayoutManager =
                                      LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false),
                              cachedItemSize: Int = 5) =
        this.apply {
            setHasFixedSize(true)
            setItemViewCacheSize(cachedItemSize)
            isDrawingCacheEnabled = true
            drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
            setLayoutManager(layoutManager)
        }