package sample.app.klarausers.widget.listener

interface OnLoadButtonClickListener {
    fun onLoadClicked()
}