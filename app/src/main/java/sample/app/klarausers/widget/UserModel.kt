package sample.app.klarausers.widget

data class UserInfoModel(val userId: String = EMPTY_STRING,
                         private val firstName: String = EMPTY_STRING,
                         private val lastName: String = EMPTY_STRING,
                         val phoneNumber: String = EMPTY_STRING,
                         val emailAddress: String = EMPTY_STRING,
                         val profileImageUrl: String = EMPTY_STRING
) : InfoModel(UserModelType.DISPLAYING) {

    val fullUserName = firstName + lastName

    val hasId: Boolean = userId.isNotBlank()
    val hasName: Boolean = fullUserName.isNotBlank()
    val hasPhone: Boolean = phoneNumber.isNotBlank()
    val hasEmail: Boolean = emailAddress.isNotBlank()
    val hasProfileImage: Boolean = profileImageUrl.isNotBlank()

    val isDefault: Boolean = !hasId && !hasName && !hasPhone && !hasEmail && !hasProfileImage
}

object UserInfoLoadingModel : InfoModel(UserModelType.LOADING)

abstract class InfoModel(val type: UserModelType)

sealed class UserModelType {
    object LOADING : UserModelType()
    object DISPLAYING : UserModelType()
}

private const val EMPTY_STRING = ""