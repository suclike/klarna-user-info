package sample.app.klarausers.widget.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import com.squareup.picasso.Picasso
import sample.app.klarausers.R
import sample.app.klarausers.widget.InfoModel
import sample.app.klarausers.widget.UserInfoModel
import sample.app.klarausers.widget.UserModelType
import sample.app.klarausers.widget.listener.OnDeleteButtonClickListener
import sample.app.klarausers.widget.listener.OnLoadButtonClickListener

class UsersRecyclerViewAdapter(private val onLoadClickListener: OnLoadButtonClickListener,
                               private val onDeleteButtonClickListener: OnDeleteButtonClickListener
) : RecyclerView.Adapter<AbstractViewHolder>() {

    companion object {
        fun createAdapter(onLoadClickListener: OnLoadButtonClickListener,
                          onDeleteButtonClickListener: OnDeleteButtonClickListener)
                : UsersRecyclerViewAdapter =
                UsersRecyclerViewAdapter(onLoadClickListener,
                        onDeleteButtonClickListener)
    }

    private var listOfModels: MutableList<InfoModel> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        return when (viewType) {
            VIEW_LOAD_INFO_TYPE -> LoadingViewHolder.createViewHolder(parent, onLoadClickListener)
            else -> PersonViewHolder.createViewHolder(parent, onDeleteButtonClickListener)
        }
    }

    override fun getItemCount(): Int = listOfModels.size

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.bind(listOfModels[position])
    }

    override fun getItemViewType(position: Int): Int {
        return when (listOfModels[position].type) {
            UserModelType.LOADING -> VIEW_LOAD_INFO_TYPE
            else -> VIEW_LIST_INFO_TYPE
        }
    }

    fun setAdapterDate(newListOfModels: List<InfoModel>) {
        this.listOfModels.clear()
        this.listOfModels.addAll(newListOfModels)
        notifyDataSetChanged()
    }

    fun updateItems(userInfo: UserInfoModel) {
        swapLastItemWithContent(userInfo)
    }

    fun removeExistingUser() {
        replaceExistingInfoWithEmpty()
    }

    private fun swapLastItemWithContent(userInfo: UserInfoModel) {
        listOfModels[getLastItem()] = userInfo
        notifyItemChanged(getLastItem(), userInfo)
    }

    private fun replaceExistingInfoWithEmpty() {
        val emptyInfoItem = UserInfoModel()
        listOfModels[getLastItem()] = emptyInfoItem
        notifyItemChanged(getLastItem(), emptyInfoItem)
    }

    internal fun getLastItem() = if (itemCount == 0) 0 else itemCount - 1
}

internal class PersonViewHolder(itemView: View,
                                private val onDeleteButtonClickListener: OnDeleteButtonClickListener
) : AbstractViewHolder(itemView) {

    companion object {
        fun createViewHolder(viewGroup: ViewGroup,
                             onDeleteButtonClickListener: OnDeleteButtonClickListener): PersonViewHolder {
            return PersonViewHolder(viewGroup.inflate(R.layout.user_info_view), onDeleteButtonClickListener)
        }
    }

    private val imagePlaceHolder: Int by lazy {
        R.drawable.ic_user_placeholder_male
    }

    private val userFirstLastNameTxt: TextView? = itemView.findViewById(R.id.userFirstLastNameTxt)
    private val phoneNumberTxt: TextView? = itemView.findViewById(R.id.userPhoneNumberTxt)
    private val emailAddressTxt: TextView? = itemView.findViewById(R.id.userEmailTxt)
    private val profileImage: ImageView? = itemView.findViewById(R.id.userProfileImg)

    private val deleteButton: Button? = itemView.findViewById(R.id.deleteUserBtn)
    private val loadingProgress: LottieAnimationView? = itemView.findViewById(R.id.dataSettingAnimationView)

    override fun bind(infoModel: InfoModel) {
        infoModel as UserInfoModel
        with(infoModel) {
            showAnimatedProgress(infoModel)
            setName(infoModel)
            setPhone(infoModel)
            setEmail(infoModel)
            setProfileImage(infoModel)
            setupDeleteButton(infoModel)
        }
    }

    private fun setName(infoModel: UserInfoModel) {
        with(infoModel) {
            userFirstLastNameTxt?.let { view ->
                if (hasName) {
                    setSelectedText(view, fullUserName)
                } else {
                    setNonSelectedText(view, getPlaceholderText(R.string.name_placeholder_txt))
                }
            }
        }
    }

    private fun setEmail(infoModel: UserInfoModel) {
        with(infoModel) {
            emailAddressTxt?.let { view ->
                if (hasEmail) {
                    setSelectedText(view, emailAddress)
                } else {
                    setNonSelectedText(view, getPlaceholderText(R.string.email_placeholder_txt))
                }
            }
        }
    }

    private fun setPhone(infoModel: UserInfoModel) {
        with(infoModel) {
            phoneNumberTxt?.let { view ->
                if (hasPhone) {
                    setSelectedText(view, phoneNumber)
                } else {
                    setNonSelectedText(view, getPlaceholderText(R.string.phone_placeholder_txt))
                }
            }
        }
    }

    private fun setupDeleteButton(infoModel: UserInfoModel) {
        when {
            infoModel.isDefault -> deleteButton?.isEnabled = false
            else -> {
                deleteButton?.isEnabled = true
                deleteButton?.setOnClickListener {
                    showAnimatedProgress(infoModel)
                    onDeleteButtonClickListener.onDeleteClicked(infoModel.userId)
                }
            }
        }
    }

    private fun setProfileImage(infoModel: UserInfoModel) {
        with(infoModel) {
            profileImage?.let { imView ->
                if (hasProfileImage) {
                    profileImageUrl.let { url ->
                        if (url.isNotEmpty()) {
                            imView.post {
                                loadImageToView(url, imView)
                            }
                        }
                    }
                } else {
                    imView.setImageResource(imagePlaceHolder)
                }
            }
        }
    }

    private fun showAnimatedProgress(infoModel: UserInfoModel) {
        if (!infoModel.isDefault) {
            showLoadingAnimation()
            itemView.postDelayed({
                hideLoadingAnimation()
            }, 1000)
        } else {
            hideLoadingAnimation()
        }
    }

    private fun showLoadingAnimation() {
        loadingProgress?.let { view ->
            view.visibility = View.VISIBLE
            view.playAnimation()
        }
    }

    private fun hideLoadingAnimation() {
        loadingProgress?.let { view ->
            view.visibility = View.GONE
            if (view.isAnimating) {
                view.cancelAnimation()
            }
        }
    }

    private fun loadImageToView(url: String, imView: ImageView) {
        Picasso.get()
                .load(url)
                .noFade()
                .placeholder(imagePlaceHolder)
                .error(imagePlaceHolder)
                .into(imView)
    }

    private fun getPlaceholderText(resString: Int): String? {
        return itemView.getResString(resString)
    }

    private fun setSelectedText(view: TextView, textToShow: String) {
        view.apply {
            setCustomTextColor(R.color.filledTextColor)
            text = textToShow
        }
    }

    private fun setNonSelectedText(view: TextView, textToShow: String?) {
        view.apply {
            setCustomTextColor(R.color.nonFilledTextColor)
            text = textToShow
        }
    }
}

internal class LoadingViewHolder(itemView: View,
                                 private val onLoadClickListener: OnLoadButtonClickListener
) : AbstractViewHolder(itemView) {

    companion object {
        fun createViewHolder(viewGroup: ViewGroup,
                             onLoadClickListener: OnLoadButtonClickListener): LoadingViewHolder {
            return LoadingViewHolder(viewGroup.inflate(R.layout.load_info_layout), onLoadClickListener)
        }
    }

    private val loadUsersInfo: Button? = itemView.findViewById(R.id.loadUsersInfoBtn)

    override fun bind(infoModel: InfoModel) {
        loadUsersInfo?.setOnClickListener {
            onLoadClickListener.onLoadClicked()
        }
    }
}

abstract class AbstractViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(infoModel: InfoModel)
}

private const val VIEW_LOAD_INFO_TYPE = 0
private const val VIEW_LIST_INFO_TYPE = 1

private fun ViewGroup.inflate(layoutRes: Int): View =
        LayoutInflater.from(context).inflate(layoutRes, this, false)

private fun View.getResString(stringRes: Int): String? = this.context.getString(stringRes)

private fun TextView.setCustomTextColor(resColor: Int) =
        this.setTextColor(this.context.resources.getColor(resColor))