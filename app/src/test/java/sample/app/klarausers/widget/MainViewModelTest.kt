package sample.app.klarausers.widget

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import sample.app.klarausers.RxImmediateSchedulerRule
import sample.app.klarausers.domain.DeleteUserInfoAction
import sample.app.klarausers.domain.GetUserInfoAction

class MainViewModelTest {

    @Rule
    @JvmField
    val rule: MockitoRule = MockitoJUnit.rule();

    @Rule
    @JvmField
    var schedulersExecutorRule = RxImmediateSchedulerRule()

    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val getAllUsersAction: GetUserInfoAction = mock(GetUserInfoAction::class.java)
    private val deleteUserInfoAction: DeleteUserInfoAction = mock(DeleteUserInfoAction::class.java)

    private val userLoadObserver: Observer<UserInfoModel> = com.nhaarman.mockito_kotlin.mock()
    private val userRemovalObserver: Observer<Boolean> = com.nhaarman.mockito_kotlin.mock()
    private val errorObserver: Observer<String> = com.nhaarman.mockito_kotlin.mock()

    private lateinit var mainViewModel: MainViewModel

    @Before
    fun setup() {
        mainViewModel = MainViewModel(getAllUsersAction, deleteUserInfoAction)
    }

    @Test
    fun check_correct_methods_call_when_fetch_empty_user() {
        whenever(getAllUsersAction.getUserInfo()).thenReturn(Single.just(UserInfoModel()))

        with(mainViewModel) {
            subscribeWhenUserLoaded().observeForever(userLoadObserver)
            subscribeWhenErrorOccurred().observeForever(errorObserver)
            subscribeWhenUserRemoved().observeForever(userRemovalObserver)
            fetchRemoteUserInfo()
        }

        verify(userLoadObserver).onChanged(UserInfoModel())
        verifyZeroInteractions(userRemovalObserver)
        verifyZeroInteractions(errorObserver)
    }

    @Test
    fun check_correct_methods_call_when_remove_user() {
        whenever(deleteUserInfoAction.deleteUser("1234")).thenReturn(Single.just(true))

        with(mainViewModel) {
            subscribeWhenUserRemoved().observeForever(userRemovalObserver)
            deleteUser("1234")
        }

        verify(userRemovalObserver).onChanged(true)
        verifyZeroInteractions(userLoadObserver)
        verifyZeroInteractions(errorObserver)
    }

    @Test
    fun check_correct_methods_call_when_remove_empty_user() {
        whenever(deleteUserInfoAction.deleteUser("")).thenReturn(Single.just(false))

        with(mainViewModel) {
            subscribeWhenUserRemoved().observeForever(userRemovalObserver)
            deleteUser("")
        }

        verify(userRemovalObserver).onChanged(false)
        verifyZeroInteractions(userLoadObserver)
        verifyZeroInteractions(errorObserver)
    }
}