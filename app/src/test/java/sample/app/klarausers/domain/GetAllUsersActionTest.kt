package sample.app.klarausers.domain

import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import sample.app.klarausers.RxSchedulersOverride
import sample.app.klarausers.remote.api.UserInfoDto
import sample.app.klarausers.remote.config.Success
import sample.app.klarausers.remote.repository.GetUserInfoRepository

class GetAllUsersActionTest {

    @Rule
    @JvmField
    val rule: MockitoRule = MockitoJUnit.rule();

    @Rule
    @JvmField
    var schedulersExecutorRule = RxSchedulersOverride()

    private val getAllUsersRepository: GetUserInfoRepository = Mockito.mock(GetUserInfoRepository::class.java)

    private lateinit var getAllUsersAction: GetUserInfoAction

    @Before
    fun setup() {
        getAllUsersAction = GetUserInfoAction(getAllUsersRepository)
    }

    @Test
    fun check_correct_response_when_fetching_user() {
        whenever(getAllUsersRepository.getUsersInfo()).thenReturn(Single.just(Success(validUserInfoDto)))

        getAllUsersAction.getUserInfo()
                .test()
                .apply {
                    assertNoErrors()
                    assertValue { currenValue ->
                        currenValue.hasEmail && currenValue.hasId && currenValue.hasName
                                && currenValue.hasPhone && currenValue.hasProfileImage
                    }
                }
    }

    @Test
    fun check_correct_response_when_fetching_nulls_for_user() {
        whenever(getAllUsersRepository.getUsersInfo()).thenReturn(Single.just(Success(nullsUserInfoDto)))

        getAllUsersAction.getUserInfo()
                .test()
                .apply {
                    assertNoErrors()
                    assertValue { currenValue ->
                        currenValue.isDefault
                    }
                }
    }
}

private val validUserInfoDto: UserInfoDto = UserInfoDto("10-11-12", "test", "me",
        "1234567890", "test@test.com", "url://")
private val nullsUserInfoDto: UserInfoDto = UserInfoDto(null, null, null,
        null, null, null)