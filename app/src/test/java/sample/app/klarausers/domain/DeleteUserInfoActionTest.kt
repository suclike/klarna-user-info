package sample.app.klarausers.domain

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import sample.app.klarausers.RxSchedulersOverride
import sample.app.klarausers.remote.config.Failure
import sample.app.klarausers.remote.config.Success
import sample.app.klarausers.remote.repository.DeleteUserInfoRepository

class DeleteUserInfoActionTest {

    @Rule
    @JvmField
    val rule: MockitoRule = MockitoJUnit.rule();

    @Rule
    @JvmField
    var schedulersExecutorRule = RxSchedulersOverride()

    private val deleteUserInfoRepository: DeleteUserInfoRepository = mock(DeleteUserInfoRepository::class.java)

    private lateinit var deleteUserInfoAction: DeleteUserInfoAction

    @Before
    fun setup() {
        deleteUserInfoAction = DeleteUserInfoAction(deleteUserInfoRepository)
    }

    @Test
    fun check_correct_response_when_remove_user() {
        whenever(deleteUserInfoRepository.deleteUser("")).thenReturn(Single.just(Success(any())))

        deleteUserInfoAction.deleteUser("")
                .test()
                .apply {
                    assertNoErrors()
                    assertValue(true)
                }
    }

    @Test
    fun check_correct_response_when_fail_remove_user() {
        whenever(deleteUserInfoRepository.deleteUser("")).thenReturn(Single.just(Failure(any())))

        deleteUserInfoAction.deleteUser("")
                .test()
                .apply {
                    assertNoErrors()
                    assertValue(false)
                }
    }
}