# Klarna User Info

This is Kotlin written sample applications based on a given public API: 
https://test16231.docs.apiary.io/#reference

Feature set of application:

 - fetch and display user details with GET requiest: 
https://test16231.docs.apiary.io/#reference/0/get-user-profiles/get-user

 - remove existing user by it's id 
 https://test16231.docs.apiary.io/#reference/0/delete-user-profile/delete-user

Project is build with gradle; use following commands 

 - to install app:
 ./gradlew clean installDebug
 
 - to run JUnit tests 
 ./gradlew clean testDebugUnitTest